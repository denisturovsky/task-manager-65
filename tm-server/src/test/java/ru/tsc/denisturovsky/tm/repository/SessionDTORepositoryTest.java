package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.SessionDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.SessionTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static SessionDTORepository REPOSITORY = CONTEXT.getBean(SessionDTORepository.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        USER_SESSION3.setUserId(USER_ID);
        Assert.assertNotNull(REPOSITORY.save(USER_SESSION3));
        @Nullable final Optional<SessionDTO> session = REPOSITORY.findByUserIdAndId(USER_ID, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.get().getId());
        Assert.assertEquals(USER_ID, session.get().getUserId());
    }

    @After
    public void after() throws Exception {
        REPOSITORY.deleteAll();
    }

    @Before
    public void before() throws Exception {
        USER_SESSION1.setUserId(USER_ID);
        USER_SESSION2.setUserId(USER_ID);
        REPOSITORY.save(USER_SESSION1);
        REPOSITORY.save(USER_SESSION2);
    }

    @Test
    public void clearByUserId() throws Exception {
        REPOSITORY.deleteByUserId(USER_ID);
        Assert.assertEquals(0, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(REPOSITORY.existByUserIdAndId(USER_ID, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(REPOSITORY.existByUserIdAndId(USER_ID, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), REPOSITORY.findByUserId(""));
        final List<SessionDTO> sessions = REPOSITORY.findByUserId(USER_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(USER_ID, session.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final Optional<SessionDTO> session = REPOSITORY.findByUserIdAndId(USER_ID, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.get().getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertEquals(2, REPOSITORY.findByUserId(USER_ID).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        REPOSITORY.deleteByUserIdAndId(USER_ID, USER_SESSION2.getId());
        Assert.assertEquals(Optional.empty(), REPOSITORY.findByUserIdAndId(USER_ID, USER_SESSION2.getId()));
    }

}