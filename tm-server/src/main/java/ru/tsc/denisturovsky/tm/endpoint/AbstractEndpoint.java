package ru.tsc.denisturovsky.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.denisturovsky.tm.api.service.IServiceLocator;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.request.AbstractUserRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.exception.EndpointException;
import ru.tsc.denisturovsky.tm.exception.user.AccessDeniedException;

@Getter
@Setter
@Controller
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    @Autowired
    private IServiceLocator serviceLocator;

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request
    ) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable SessionDTO session;
        try {
            session = serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
