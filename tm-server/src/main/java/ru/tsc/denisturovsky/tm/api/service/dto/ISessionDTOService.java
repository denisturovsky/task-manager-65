package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

}
