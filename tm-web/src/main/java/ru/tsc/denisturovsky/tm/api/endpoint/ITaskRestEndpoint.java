package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRestEndpoint {

    void clear() throws Exception;

    long count() throws Exception;

    void delete(@NotNull TaskDTO project) throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    @Nullable
    TaskDTO findById(@NotNull String id) throws Exception;

    @NotNull
    TaskDTO save(@NotNull TaskDTO project) throws Exception;

}