package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

}
