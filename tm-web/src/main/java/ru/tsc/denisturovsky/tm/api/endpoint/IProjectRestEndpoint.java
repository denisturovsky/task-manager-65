package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRestEndpoint {

    void clear() throws Exception;

    long count() throws Exception;

    void delete(@NotNull ProjectDTO project) throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @Nullable
    ProjectDTO findById(@NotNull String id) throws Exception;

    @NotNull
    ProjectDTO save(@NotNull ProjectDTO project) throws Exception;

}