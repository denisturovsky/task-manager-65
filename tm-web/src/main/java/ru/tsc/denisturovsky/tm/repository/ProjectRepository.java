package ru.tsc.denisturovsky.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.denisturovsky.tm.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}