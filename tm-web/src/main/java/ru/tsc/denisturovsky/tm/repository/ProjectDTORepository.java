package ru.tsc.denisturovsky.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
