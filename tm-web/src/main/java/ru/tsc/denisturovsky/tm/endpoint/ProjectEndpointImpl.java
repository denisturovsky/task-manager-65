package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.denisturovsky.tm.api.endpoint.IProjectRestEndpoint;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    public void clear() throws Exception {
        projectService.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.count();
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull ProjectDTO project) throws Exception {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull String id) throws Exception {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws Exception {
        return (projectService.findOneById(id) == null);
    }

    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") @NotNull final String id) throws Exception {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public ProjectDTO save(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        return projectService.update(project);
    }

}