package ru.tsc.denisturovsky.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@NotNull Task model) throws Exception;

    void changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    @NotNull
    Task create(@Nullable String name) throws Exception;

    @NotNull
    Task create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<Task> findAll() throws Exception;

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    Task findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable Task model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable Task model) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

}